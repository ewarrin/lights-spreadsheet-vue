import './firebase';

import Vue from 'vue';
import VueFire from 'vuefire';
import App from './App.vue'
import router from './router'

import './registerServiceWorker'
import './../node_modules/bulma/css/bulma.css'

Vue.use(VueFire);
Vue.config.productionTip = false
Vue.use(require('vue-moment'))

new Vue({
    router,
    render: function (h) { return h(App) }
}).$mount('#app')