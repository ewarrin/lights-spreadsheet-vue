# lights-spreadsheet-app

Web app based on Sebastian Marshalls Lights Spreadsheet.

VueJS with Firebase database.

Download tasks.json file and import it into Firebase database for structure.

___

## ROADMAP 

~~1. Admin to create tasks~~
2. Add "are you sure?" modal on deleteTask 
3. Close all open dropdowns on click outside
4. Disable all future dates
5. Add stats
6. Add "advice" notifications
7. Add ability to sign up.

___

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

